//
// Created by tim on 6/16/17.
//

#ifndef VISITABLE_H
#define VISITABLE_H

#include "Visitor.h"

template <class TVisitable>
class Visitable
{
public:
    template <typename T>
    void accept(T & visitor)
    {
        visitor.visit(static_cast<TVisitable &>(*this));
    }
};

#endif //VISITABLE_H
