#include <iostream>
#include "Visitable.h"


class This: public Visitable<This>
{
public:
    std::string toString(){
        return "This";
    }
};

class That: public  Visitable<That>
{
public:
    std::string toString(){
        return "That";
    }
};

class Composite: public Visitable<Composite>
{
public:
    This const &get_this() {
        return _this;
    }

    void set_this(This &_this) {
        Composite::_this = _this;
    }

    That const &get_that() {
        return _that;
    }

    void set_that(That &_that) {
        Composite::_that = _that;
    }

    std::string toString(){
        return "Composite";
    }
private:
    This _this;
    That _that;
};

class CompositeVisitor: public  BaseVisitor, Visitor<Composite>, public Visitor<This>, public  Visitor<That>
{
public:
    void visit(Composite &v){
        std::cout<< getName()+" visited " + v.toString() <<std::endl;
        That that = v.get_that();
        visit(that);
        This _this = v.get_this();
        visit(_this);

    }
    void visit(This &v){
        std::cout<<getName()+" visited " + v.toString() << std::endl;
    }
    void visit(That &v){
        std::cout<<getName()+" visited " + v.toString() << std::endl;
    }

    std::string getName(){
        return "CompositeVisitor";
    }
};


int main() {
    This *_this = new This();
    That *_that = new That();
    Composite *composite= new Composite();
    composite->set_that(*_that);
    composite->set_this(*_this);

    CompositeVisitor *v2 = new CompositeVisitor;
    composite->accept(*v2);

    return 0;
}